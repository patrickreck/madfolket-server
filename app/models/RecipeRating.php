<?php

class RecipeRating extends Eloquent {

	protected $table = 'recipes_ratings';

	protected $hidden = array('created_at', 'updated_at');

	protected $fillable = array('rating');

	public function recipe() {
		return $this->belongsTo('Recipe');
	}

	public function user() {
		return $this->belongsTo('User');
	}

	public function notifications()
    {
        return $this->morphMany('Notification', 'notifiable');
    }

}