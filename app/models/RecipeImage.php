<?php

class RecipeImage extends Eloquent {

	protected $table = 'recipes_images';

	protected $hidden = array('created_at', 'updated_at', 'recipe_id');

	public function recipe() {
		return $this->belongsTo('Recipe');
	}
}

?>