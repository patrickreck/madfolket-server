<?php

class Origin extends Eloquent {

	protected $table = 'origins';

	protected $hidden = array('created_at', 'updated_at');
	
	public function recipes() {
		return $this->belongsToMany('Recipe');
	}
}

?>