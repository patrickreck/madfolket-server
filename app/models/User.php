<?php

class User extends Eloquent {

	protected $table = 'users';

	protected $appends = array('recipes_count', 'badges_short');

	protected $hidden = array('password', 'email', 'permissions', 'activated', 'activation_code', 'activated_at', 'persist_code', 'reset_password_code', 'updated_at');

	public function recipes() {
		return $this->hasMany('Recipe');
	}

	public function shoppingLists() {
		return $this->belongsToMany('ShoppingList', 'shopping_lists_users');
	}

	public function getRecipesCountAttribute() {
		return count($this->recipes()->get());
	}

	public function badges() {
		return $this->belongsToMany('Badge', 'users_badges')->withTimestamps();
	}

	public function getBadgesAttribute() {
		$badges = $this->badges()->get()->toArray();

		foreach ($badges as $index => $badge) {
			$badges[$index]['created_at'] = $badges[$index]['pivot']['created_at'];
			unset($badges[$index]['pivot']);
		}

		return array_reverse($badges);
	}

	public function getBadgesShortAttribute() {
		$badges = $this->badges()->get();
		
		$gold = 0;
		$silver = 0;
		$bronze = 0;
		
		foreach ($badges as $badge) {
			if ($badge->type == 1) {
				$gold++;
				continue;
			}
			elseif ($badge->type == 2) {
				$silver++;
				continue;
			}
			else {
				$bronze++;
				continue;
			}
		}

		return array(
			'gold' 	 => $gold,
			'silver' => $silver,
			'bronze' => $bronze
		);
	}

	public function ratings() {
		return $this->hasMany('RecipeRating');
	}

	public function recipeComments() {
		return $this->hasMany('RecipeComment');
	}

	public function views() {
		return $this->hasMany('RecipeView');
	}

	public function grantBadge($badgeId) {
		$alreadyHaveBadge = DB::table('users_badges')
					->where('user_id', '=', $this->id)
					->where('badge_id', '=', $badgeId)
					->first();
		if (!$alreadyHaveBadge) {
			$this->badges()->attach(Badge::find($badgeId));
		}
	}

	public function notifications()
	{
	    return $this->hasMany('Notification');
	}

}
