<?php

class Category extends Eloquent {

	protected $table = 'categories';

	protected $hidden = array('created_at', 'updated_at');

	public function recipes() {
		return $this->hasMany('Recipe');
	}

}

?>