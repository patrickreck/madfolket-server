<?php

class RecipeComment extends Eloquent {

	protected $table = 'recipes_comments';

	protected $hidden = array('recipe_id');

	protected $fillable = array('comment');

	public function user() {
		return $this->belongsTo('User');
	}

	public function recipe() {
		return $this->belongsTo('Recipe');
	}

	public function reports()
    {
        return $this->morphMany('Report', 'reportable');
    }

	public function notifications()
    {
        return $this->morphMany('Notification', 'notifiable');
    }

}

?>