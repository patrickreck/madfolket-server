<?php

use Carbon\Carbon;

class Recipe extends Eloquent {

	protected $table = 'recipes';

	protected $appends = array('rating', 'myrating', 'images');

	protected $hidden = array('updated_at');

	public function getMyratingAttribute() {
		if (Sentry::check())
			return (int) RecipeRating::where('recipe_id', $this->id)->where('user_id', Sentry::getUser()->id)->first()['rating'];
		return null;
	}

	public function getImagesAttribute() {
		return $this->images()->get();
	}

	public function user() {
		return $this->belongsTo('User');
	}

	public function images() {
		return $this->hasMany('RecipeImage')->orderBy('main', 'DESC');
	}

	public function ingredients() {
		return $this->belongsToMany('Ingredient', 'recipes_ingredients')->withPivot('amount', 'unit_id');
	}

	public function steps() {
		return $this->hasMany('RecipeStep');
	}

	public function comments() {
		return $this->hasMany('RecipeComment')->orderBy('created_at', 'DESC');
	}

	public function category() {
		return $this->belongsTo('Category');
	}

	public function getRatingAttribute() {
		return round(DB::table('recipes_ratings')->where('recipe_id', '=', $this->id)->avg('rating'));
	}

	public function ratings() {
		return $this->hasMany('RecipeRating');
	}

	public function origin() {
		return $this->belongsTo('Origin');
	}

	public function views() {
		return $this->hasMany('RecipeView');
	}

	public function tags() {
		return $this->belongsToMany('Tag', 'recipes_tags');
	}

	public function addView() {

		$shouldAddView = false;
		if ($view = RecipeView::where('ip', '=', $_SERVER['REMOTE_ADDR'])->orderBy('id', 'DESC')->first()) {
			if (Carbon::now()->diffInMinutes($view->created_at) >= 15) {
				$shouldAddView = true;
			}
		} else {
			$shouldAddView = true;
		}

		if ($shouldAddView) {
			$view = new RecipeView(array('ip' => $_SERVER['REMOTE_ADDR']));
			if (Sentry::check()) {
				$view->user_id = Sentry::getUser()->id;
			}
			$this->views()->save($view);			
		}

	}

	public function reports()
    {
        return $this->morphMany('Report', 'reportable');
    }

	public function notifications()
    {
        return $this->morphMany('Notification', 'notifiable');
    }

}

?>