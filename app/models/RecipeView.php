<?php

class RecipeView extends Eloquent {

	protected $table = 'recipes_views';

	protected $fillable = array('recipe_id', 'ip');

}