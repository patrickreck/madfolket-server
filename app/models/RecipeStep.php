<?php

class RecipeStep extends Eloquent {
	protected $table = 'recipes_steps';

	protected $hidden = array('created_at', 'updated_at', 'recipe_id');

}

?>