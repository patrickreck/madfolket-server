<?php

class Ingredient extends Eloquent {

	protected $appends = array('amount', 'unit', 'purchased');

	protected $hidden = array('created_at', 'updated_at', 'pivot', 'unit_id');


	public function unit() {
		return $this->belongsTo('Unit');
	}

	public function getAmountAttribute() {
		if (isset($this->pivot)) {
			return $this->pivot->amount + 0;
		}
	}

	public function getPurchasedAttribute() {
		if (isset($this->pivot) && isset($this->pivot->purchased)) {
			$purchased = $this->pivot->purchased;
			if ($purchased == 0) {
				return false;
			}
			return true;
		}
	}

	public function getUnitIdAttribute() {
		if (isset($this->pivot)) {
			return $this->unit_id = $this->pivot->unit_id;
		}
	}

	public function getUnitAttribute() {
		return $this->unit = Unit::find($this->unit_id);
	}


}

?>