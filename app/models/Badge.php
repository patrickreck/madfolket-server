<?php

class Badge extends Eloquent {

	protected $table = 'badges';

	protected $hidden = array('updated_at');

	public function users() {
		return $this->belongsToMany('User', 'users_badges');
	}

	public function notifications()
    {
        return $this->morphMany('Notification', 'notifiable');
    }
}

?>