<?php

class Report extends Eloquent {

	protected $fillable = array('description');

    public function reportable()
    {
        return $this->morphTo();
    }

    public function User()
    {
        return $this->belongsTo('User');
    }
}