<?php

class TagCategory extends Eloquent {

	protected $fillable = array('name');

	protected $table = 'tag_categories';

	protected $hidden = array('id', 'created_at', 'updated_at');

	public $timestamps = false;

	public function tags() {
		return $this->hasMany('Tag');
	}

}

?>