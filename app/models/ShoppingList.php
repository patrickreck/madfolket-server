<?php

class ShoppingList extends Eloquent {

	protected $appends = array('recipes');

	protected $fillable = array('name');

	public function getRecipesAttribute() {
		return $this->recipes()->get();
	}

	public function recipes() {
		return $this->belongsToMany('Recipe', 'shopping_lists_recipes')->withPivot('persons');
	}

	public function ingredients() {
		return $this->belongsToMany('Ingredient', 'shopping_lists_ingredients')->withPivot('amount', 'unit_id', 'purchased');
	}

	public function users() {
		return $this->belongsToMany('User', 'shopping_lists_users');
	}

	public function isOwner($user) {
		 if (DB::table('shopping_lists_users')
			->where('user_id', $user->id)
			->where('shopping_list_id', $this->id)
			->limit(1)
			->count())
			return true;
		return false;
	}
}

?>