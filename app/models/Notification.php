<?php

class Notification extends Eloquent
{

    public function notifiable()
    {
        return $this->morphTo();
    }
 
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function sender()
	{
	    return $this->belongsTo('User', 'sender_id');
	}
	 
	public function from($user)
	{
	    $this->sender()->associate($user);
	 
	    return $this;
	}

	public function to($user)
	{
	    $this->user()->associate($user);
	 
	    return $this;
	}
	 
	public function regarding($object)
	{
	    $this->notifiable()->associate($object);
	}

	public function scopeUnread($query)
	{
	    return $query->where('is_read', '=', 0);
	}

}

?>