<?php

class Tag extends Eloquent {

	protected $fillable = array('name');

	protected $hidden = array('tag_category_id', 'created_at', 'updated_at');

	public function tagCategory() {
		return $this->belongsTo('TagCategory');
	}

	public function recipes() {
		return $this->belongsToMany('Recipe', 'recipes_tags');
	}
}