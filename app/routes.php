<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

App::before(function($request)
{
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
    header('Access-Control-Allow-Credentials: true');
});

Route::controller('users', 'UserController');
Route::controller('recipes', 'RecipeController');
Route::controller('authentication', 'AuthenticationController');
Route::controller('test', 'TestController');
Route::controller('notifications', 'NotificationController');
Route::controller('shopping-lists', 'ShoppingListController');
Route::controller('reports', 'ReportController');

Route::get('/origins', function() {
	return Origin::get();
});

Route::get('/tags', function() {
	return TagCategory::with('tags')->get();
});

Route::get('/categories', function() {
	return Category::get();
});

Route::get('/units', function() {
	return Unit::get();
});

Route::get('/ingredients/{ingredient}', function($ingredient) {
	if ($ingredient) {
		return Ingredient::with('unit')->where('name', 'LIKE', '%'.$ingredient.'%')->take(10)->get();
	}
});

Route::get('/', function()
{
	return $string = str_random(40);
	return "Vi klarede det!";

});

