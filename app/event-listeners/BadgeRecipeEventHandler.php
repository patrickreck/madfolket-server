<?php

class BadgeRecipeEventHandler {

	private $recipes;
	private $recipesCount;

	public function handle($user) {

		sleep(1);

		$this->recipes = $user->recipes;
		$this->recipesCount = count($this->recipes);

		// Opret din første opskrift
		if ($this->recipesCount > 0) {
			$user->grantBadge(1);
		}
	
		// Opret 5 opskrifter
		if ($this->recipesCount >= 5) {
			$user->grantBadge(2);
		}

		// Opret 20 opskrifter
		if ($this->recipesCount >= 20) {
			$user->grantBadge(3);
		}

		// Opret 50 opskrifter
		if ($this->recipesCount >= 50) {
			$user->grantBadge(4);
		}

		// Fin opskrift (3 stjerner, 10 stemmer)
		foreach ($this->recipes as $recipe) {
			if ($this->hasRatingAndVotes($recipe, 3, 10)) {
				$user->grantBadge(5);
				break;
			}
		}

		// God opskrift (4 stjerner, 30 stemmer)
		foreach ($this->recipes as $recipe) {
			if ($this->hasRatingAndVotes($recipe, 4, 30)) {
				$user->grantBadge(6);
				break;
			}
		}

		// Super opskrift (4 stjerner, 100 stemmer)
		foreach ($this->recipes as $recipe) {
			if ($this->hasRatingAndVotes($recipe, 4, 100)) {
				$user->grantBadge(7);
				break;
			}
		}

		// Kok (5 opskrifter med mindst 3 stjerner, med mindst 10 stemmer)
		if ($this->recipesCount >= 5) {
			$hits = 0;

			foreach ($this->recipes as $recipe) {
				if ($this->hasRatingAndVotes($recipe, 3, 10))
					$hits++;

				if ($hits == 5) {
					$user->grantBadge(8);
					break;
				}
			}
		}

		// Mesterkok (10 opskrifter med mindst 4 stjerner, med mindst 10 stemmer)
		if ($this->recipesCount >= 10) {
			$hits = 0;

			foreach ($this->recipes as $recipe) {
				if ($this->hasRatingAndVotes($recipe, 4, 10))
					$hits++;

				if ($hits == 10) {
					$user->grantBadge(9);
					break;
				}
			}
		}

		// Stjernekok (10 opskrifter med mindst 4 stjerner, med mindst 10 stemmer)
		if ($this->recipesCount >= 10) {
			$hits = 0;

			foreach ($this->recipes as $recipe) {
				if ($this->hasRatingAndVotes($recipe, 5, 15))
					$hits++;

				if ($hits == 10) {
					$user->grantBadge(10);
					break;
				}
			}
		}

		// Forfatter (Hav 5 opskrifter der er i andres kogebøger)
		// $user->grantBadge(11);

		// Opskrift Visninger
		foreach ($this->recipes as $recipe) {
			$views = count($recipe->views);
			// Bemærkelsesværdig opskrift (1000 sidevisninger)
			if ($views >= 1000) {
				$user->grantBadge(12);
			}

			// Populær opskrift (10.000 sidevisninger)
			if ($views >= 10000) {
				$user->grantBadge(13);
			}

			// Kendt opskrift (100.000 sidevisninger)
			if ($views >= 100000) {
				$user->grantBadge(14);
			}
		}

		// Biografi (Udfyld din profil)
		// $user->grantBadge(15);

		// Deltager (Skriv 10 kommentarer)
		// $user->grantBadge(16);

		// Kommentator (Skriv 15 kommentarer med 5 positive stemmer)
		// $user->grantBadge(17);

		// Portræt (Upload et profilbillede)
		// $user->grantBadge(18);

	}

	private function hasRatingAndVotes($recipe, $rating, $votes) {
		if ($recipe->rating >= $rating && count($recipe->ratings) >= $votes) {
			return true;
		}
		return false;
	}

}

?>