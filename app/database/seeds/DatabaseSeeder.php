<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->command->info('Starting seed');

		/* ------ BRUGERE ------ */

		// Lav et par brugere
	    Sentry::register(array(
	    	'first_name' => 'Patrick',
	    	'last_name'  => 'Reck',
	        'email'    	 => 'reckpatrick@gmail.com',
	        'password' 	 => 'kodeord',
	        'profile_picture' => '1.jpg',
	        'facebook_id'	=> '10152775986019410',
	        'activated'	 => 1,
	    ));
	    $user1 = User::find(1);

	    Sentry::register(array(
	    	'first_name' => 'Nicklas',
	    	'last_name'  => 'Frank',
	        'email'    	 => 'admin@cs-e.dk',
	        'password' 	 => 'kodeord',
	        'profile_picture' => '1.jpg',
	        'activated'	 => 1,
	    ));
	    $user2 = User::find(2);

		$badge1 = Badge::create(array(
			'type' 		  => '3',
			'name' 		  => 'Første opskrift',
			'description' => 'Tilføj din første opskrift'
		));

		$badge2 = Badge::create(array(
			'type' 		  => '1',
			'name' 		  => 'Vist 1000 gange',
			'description' => 'Få en af dine opskrifter vist 1000 gange'
		));

		$user1->badges()->attach($badge1);
		$user1->badges()->attach($badge2);

		/* ------ ORIGINS ------ */

		// Tilføj en række oprindelseslande
		$origins = array(
			'Danmark',
			'Tyskland',
			'Belgien',
			'Bulgarien',
			'Cypern',
			'Estland',
			'Finland',
			'Frankrig',
			'Grækenland',
			'Irland',
			'Italien',
			'Kroatien',
			'Letland',
			'Litauen',
			'Luxemborg',
			'Malta',
			'Holland',
			'Polen',
			'Portugal',
			'Rumænien',
			'Slovakiet',
			'Slovenien',
			'Spanien',
			'Sverige',
			'Tjekkiet',
			'Ungarn',
			'Østrig',
			'Irak',
			'Iran',
			'Israel',
		);

		foreach ($origins as $origin) {
			Origin::create(array(
				'name' => $origin
			));
		}


		/* ------ OCCASIONS ------ */

		$occasions = array(
			'Jul',
			'Påske',
			'Pinse',
			'Nytår',
			'Fødselsdag',
		);

		foreach ($occasions as $occasion) {
			Occasion::create(array(
				'name' => $occasion
			));
		}

		/* ------ UNITS ------ */

		$units = array(
			'kilogram'   => 'kg',
			'gram'	     => 'g',
			'milligram'  => 'mg',

			'liter'		 => 'l',
			'deciliter'  => 'dl',
			'milliliter' => 'ml',

			'teske'		 => 'tsk',
			'spiseske'	 => 'spsk',
			'knivspids'	 => 'knsp',

			'styk'		 => 'stk',
			'pakke'		 => 'pk',
			'dåse'		 => 'ds',
		);

		foreach ($units as $unit => $shorthand) {
			Unit::create(array(
				'name' => $unit,
				'shorthand' => $shorthand
			));
		}

		/* ------ INGREDIENTS ------ */

		$ingredients = array(
			'Hakket oksekød 3%' => 2,
			'Minimælk'			=> 5,
			'Salt'				=> 9,

			'Lasagneplader'		=> 10,
			'Flåede tomater'	=> 12,

			'Løg'				=> 10,
			'Oregano'			=> 2,

		);

		foreach ($ingredients as $ingredient => $standard_unit) {
			Ingredient::create(array(
				'name'	  => $ingredient,
				'unit_id' => $standard_unit
			));
		}

		/* ------ CATEGORIES ------ */

		$categories = array(
			'Hovedret',
			'Forret',
			'Dessert',
			'Frokost',
			'Snack',
			'Morgenmad'
		);

		foreach ($categories as $category) {
			Category::create(array(
				'name' => $category
			));
		};

		/* ------ OPSKRIFTER ------ */

	    // Lav en opskrift
	    $recipe1 = Recipe::create(array(
	    	'name'		   => 'Lasagne',
	    	'introduction' => 'Denne lasagne er super hurtig at bikse sammen, og smager ganske vidunderligt',
	    	'hours'		   => '0',
	    	'minutes'	   => '45'
	    ));

	    // Tilføj billeder til opskriften
		$recipe_image = RecipeImage::create(array(
			'path'	=> '1.jpg'
		));
		$recipe1->images()->save($recipe_image);

		$recipe_image = RecipeImage::create(array(
			'path'	=> '2.jpg'
		));
		$recipe1->images()->save($recipe_image);

		// Tilføj en oprindelse
		$recipe1->origin()->associate(Origin::find(11));

		// Tilføj en kategori
		$recipe1->category()->associate(Category::find(1));


		// Tilføj ingredienser
		$recipe1->ingredients()->attach(Ingredient::find(1), array('amount' => 500, 'unit_id' => 2));
		$recipe1->ingredients()->attach(Ingredient::find(3), array('amount' => 1.5, 'unit_id' => 7));
		$recipe1->ingredients()->attach(Ingredient::find(2), array('amount' => 1, 'unit_id' => 5));
		$recipe1->ingredients()->attach(Ingredient::find(6), array('amount' => 3, 'unit_id' => 10));

		// Tilføj steps
		$steps = array(
			'Svits løg i en gryde',
			'Tilsæt derefter oksekødet til det er svitset brunt',
			'Tilsæt herefter de andre ting på listen lige så stille. Den skal have 20 minutter.',
			'Saml pladerne i et fad og hæld sovsen over'
		);

		foreach ($steps as $step) {
			$step_after = RecipeStep::create(array(
				'description' => $step
			));
			$recipe1->steps()->save($step_after);
		}

		// Tilføj ratings
		$rating1 = RecipeRating::create(array(
			'rating' => 5
		));
		$user1->ratings()->save($rating1);

		$rating2 = RecipeRating::create(array(
			'rating' => 1
		));
		$user2->ratings()->save($rating2);

		$recipe1->ratings()->save($rating1);
		$recipe1->ratings()->save($rating2);

		// Tilføj comments
		$comment = RecipeComment::create(array(
			'comment' => 'Den er SÅ god!',
			'created_at' => \Carbon\Carbon::now()->subHour()
		));	
		$user1->recipeComments()->save($comment);
		$recipe1->comments()->save($comment);

		$comment = RecipeComment::create(array(
			'comment' => 'Worst fucking lasagne ever'
		));
		$user2->recipeComments()->save($comment);
		$recipe1->comments()->save($comment);

		// Tilføj opskriften til en bruger
		$user1->recipes()->save($recipe1);



		// Lav en anden opskrift
	    $recipe2 = Recipe::create(array(
	    	'name'		   => 'Boller i Karry',
	    	'introduction' => 'Billederne er lånt fra Dybvad',
	    	'hours'		   => '2',
	    	'minutes'	   => '30'
	    ));

	    // Tilføj billeder til opskriften
		$recipe_image = RecipeImage::create(array(
			'path'	=> '1.jpg'
		));
		$recipe2->images()->save($recipe_image);

		$recipe_image = RecipeImage::create(array(
			'path'	=> '2.jpg'
		));
		$recipe2->images()->save($recipe_image);

		// Tilføj en oprindelse
		$recipe2->origin()->associate(Origin::find(10));

		// Tilføj en kategori
		$recipe2->category()->associate(Category::find(2));


		// Tilføj ingredienser
		$recipe2->ingredients()->attach(Ingredient::find(1), array('amount' => 500, 'unit_id' => 2));
		$recipe2->ingredients()->attach(Ingredient::find(3), array('amount' => 1.5, 'unit_id' => 7));
		$recipe2->ingredients()->attach(Ingredient::find(2), array('amount' => 1, 'unit_id' => 5));
		$recipe2->ingredients()->attach(Ingredient::find(6), array('amount' => 3, 'unit_id' => 10));

		// Tilføj steps
		$steps = array(
			'Rør farsen sej med salt',
			'Tilsæt mel, æg, revet løg og krydderier og rør sammen',
			'Rør væsken i lidt efter lidt. Farsen skal være så fast, at den kan formes til boller, lad evt. trække i køleskabet en halv times tid',
			'Bring saltvandet i kog i en bred gryde',
			'Tag gryden af varmen'
		);

		foreach ($steps as $step) {
			$step_after = RecipeStep::create(array(
				'description' => $step
			));
			$recipe2->steps()->save($step_after);
		}

		// Tilføj ratings
		$rating1 = RecipeRating::create(array(
			'rating' => 3
		));
		$user1->ratings()->save($rating1);

		$rating2 = RecipeRating::create(array(
			'rating' => 2
		));
		$user2->ratings()->save($rating2);

		$recipe2->ratings()->save($rating1);
		$recipe2->ratings()->save($rating2);

		// Tilføj comments
		$comment = RecipeComment::create(array(
			'comment' => 'Jeg kan ret godt lide Boller i Karry :-D',
			'created_at' => \Carbon\Carbon::now()->subHour()
		));	
		$user1->recipeComments()->save($comment);
		$recipe2->comments()->save($comment);

		$comment = RecipeComment::create(array(
			'comment' => 'Boller i Karry er det værste lort'
		));
		$user2->recipeComments()->save($comment);
		$recipe2->comments()->save($comment);

		// Tilføj opskriften til en bruger
		$user1->recipes()->save($recipe2);


		$this->command->info('Seed completed');
	}
}