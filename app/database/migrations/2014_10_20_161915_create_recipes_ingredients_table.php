<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesIngredientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipes_ingredients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->float('amount');
			$table->integer('ingredient_id');
			$table->integer('recipe_id');
			$table->integer('unit_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recipes_ingredients');
	}

}
