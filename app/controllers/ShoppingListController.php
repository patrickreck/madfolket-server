<?php

class ShoppingListController extends Controller {

	public function getIndex() {

		if (Sentry::check()) {

			$user = User::with('ShoppingLists')->find(Sentry::getUser()->id);

			return $user->shopping_lists;
		}

		return Response::make('You are not logged in', 401);

	}

	public function getNames() {
		if (Sentry::check()) {

			$user = User::find(Sentry::getUser()->id);
			$shoppingLists = ShoppingList::where('user_id', Sentry::getUser()->id)->orderBy('created_at', 'DESC')->get();

			return $shoppingLists;

		}

		return Response::make('You are not logged in', 401);
	}

	public function getShoppingList($shoppingListId) {
		if (Sentry::check()) {
			$shoppingList = ShoppingList::with('recipes', 'ingredients', 'users')->find($shoppingListId);
			if ($shoppingList->isOwner(User::find(Sentry::getUser()->id)))
				return $shoppingList;
		}

		return Response::make('You are not logged in', 401);
	}

	public function postSave() {

		if (Sentry::check()) {
			$validator = Validator::make(
				array(
					'name' => Input::get('name')
				),
				array(
					'name' => 'required|between:2,100'
				)
			);

			if ($validator->passes()) {
				$shoppingList = new ShoppingList(array('name' => Input::get('name')));
				$shoppingList->user_id = Sentry::getUser()->id;
				$shoppingList->save();

				return Response::make($shoppingList->id, 201);
			}
		} else
			return Response::make('You are not logged in', 401);
	}

	public function postAddRecipe() {

		if (Sentry::check()) {
			$validator = Validator::make(
				array(
					'shoppingList' => Input::get('shoppingList'),
					'recipe'       => Input::get('recipe'),
					'persons'      => Input::get('persons')
				),
				array(
					'shoppingList' => 'required|exists:shopping_lists,id',
					'recipe'       => 'required|exists:recipes,id',
					'persons'      => 'required|numeric|between:1,40'
				)
			);

			if ($validator->passes()) {

				$shoppingList = ShoppingList::find(Input::get('shoppingList'));

				if ($shoppingList->user_id == Sentry::getUser()->id) {

					$recipe = Recipe::find(Input::get('recipe'));

					$shoppingList->recipes()->attach($recipe, array('persons' => Input::get('persons')));

					foreach ($recipe->ingredients as $ingredient) {
						$shoppingList->ingredients()->attach($ingredient, array('recipe_id' => $recipe->id, 'amount' => $ingredient->amount, 'unit_id' => $ingredient->unit_id));
					}


					return Response::make('The recipe was added', 201);
				}
			}
		}

		return Response::make('You are not logged in', 401);

	}

	public function postSetPurchased() {

		if (Sentry::check()) {
			$validator = Validator::make(
				array(
					'shoppingList' => Input::get('shoppingList'),
					'ingredient'   => Input::get('ingredient'),
					'purchased'    => Input::get('purchased')
				),
				array(
					'shoppingList' => 'required|exists:shopping_lists,id',
					'ingredient'   => 'required|exists:shopping_lists_ingredients,ingredient_id',
					'purchased'    => 'required|boolean'
				)
			);

			if ($validator->passes()) {

				// Hvis flere 
				DB::table('shopping_lists_ingredients')
					->where('shopping_list_id', Input::get('shoppingList'))
					->where('ingredient_id', Input::get('ingredient'))
					->update(array('purchased' => Input::get('purchased')));

				return Response::make('Updated', 200);

			} else {
				return $validator->messages()->toJson();
			}
		}

		return Response::make('You are not logged in', 401);
	}

}