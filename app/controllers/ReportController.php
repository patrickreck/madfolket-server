<?php

class ReportController extends Controller {

	public function getReports() {
		if (!Sentry::check())
			return Response::make('You are not logged in', 401);

		return Report::with('user')->get();
	}

	public function postRecipe() {
		if ($this->checkValidation('recipes')) {
			$recipe = Recipe::find(Input::get('object'));
			$user = User::find(Sentry::getUser()->id);
			$description = Input::get('description');

			return $this->reportFactory($user, $recipe, $description);
		}
	}

	public function postComment() {
		if ($this->checkValidation('recipes_comments')) {
			$comment = RecipeComment::find(Input::get('object'));
			$user = User::find(Sentry::getUser()->id);
			$description = Input::get('description');

			return $this->reportFactory($user, $comment, $description);
		}
	}

	private function checkValidation($table) {
		if (!Sentry::check())
			return Response::make('You are not logged in', 401);

		$validator = Validator::make(
			array(
				'object_id'   => Input::get('object'),
				'description' => Input::get('description')
			),
			array(
				'object_id'   => 'required|exists:' . $table . ',id',
				'description' => 'max:1000'
			)
		);

		if ($validator->passes()) {
			return true;
		} else {
			return Response::make('Validation failed', 404);
		}
	}

	private function reportFactory($user, $object, $description) {
		$report = new Report;
		$report->description = $description;

		$report->user()->associate($user);
		$report->reportable()->associate($object);
		$report->save();

		return $report;
	}
}

?>