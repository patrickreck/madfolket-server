<?php

use Carbon\Carbon;

class TestController extends Controller {

	public function getListsUser() {
		return User::find(1)->shoppingLists;
	}

	public function getOwns() {

		$shoppingList = ShoppingList::find(2);

		$user = User::find(1);

		if ($shoppingList->isOwner($user))
			return "true";

		return "false";

	}

	public function getWhere() {
		return Recipe::where('id', 1)->get();
	}

	public function getTime() {
		$now = Carbon::now();
		$last_view = Carbon::now()->subMinutes(10);

		return $now->diffInMinutes($last_view) >= 15;
	}

	public function getView() {
		$recipe = Recipe::find(1);
		$recipe->addView();
	}

	public function getRecipeRating() {
		return count(Recipe::find(1)->ratings);
	}

	public function getEvent() {

		$user = User::find(Sentry::getUser()->id);
		$event = Event::fire('badges.recipe', array($user));

		return $event;
	}

	public function getIngredientExists() {

		$ingredient = 'banan';

		if ($found = Ingredient::where('name', 'LIKE', $ingredient)->first()) {
			return $found;
		}

		return 'Nope';


	}


	public function postSaveImage() {

		$validator = Validator::make(
			array(
				'image' => Input::file('file')
			),
			array(
				'image' => 'required|image|max:2500'
			)
		);

		if ($validator->passes()) {

			$file = Input::file('file');

			$newFileName = getGuid() . '.' . $file->getClientOriginalExtension();

			$file->move('images/recipe_images/temporary/', $newFileName);

			return Response::make(array('path' => $newFileName), 200);
		} else {
			return Response::make(array("Not an image"), 400);
		}

	}

	public function getGuid() {
		return getGuid();

	}

	public function getTags() {
		$begivenheder_kategori = new TagCategory(array('name' => 'Begivenheder'));
		$begivenheder_kategori->save();
		$begivenheder = array(
			'Bryllup',
			'Fødselsdag',
			'Barnedåb',
			'Konfirmation',
			'Begravelse',
		);

		foreach ($begivenheder as $begivenhed) {
			$begivenheder_kategori->tags()->save(new Tag(array('name' => $begivenhed)));
		}


		$hoejtider_kategori = new TagCategory(array('name' => 'Højtider'));
		$hoejtider_kategori->save();

		$hoejtider = array(
			'Jul',
			'Valentins Dag',
			'Fastelavn',
			'Påske',
			'Store Bededag',
			'Pinse',
			'Halloween',
			'Nytår'
		);

		foreach ($hoejtider as $hoejtid) {
			$hoejtider_kategori->tags()->save(new Tag(array('name' => $hoejtid)));
		}


		$allergier_kategori = new TagCategory(array('name' => 'Allergier'));
		$allergier_kategori->save();

		$allergier = array(
			'Glutenfri',
			'Laktosefri',
			'Nøddefri'
		);

		foreach ($allergier as $allergi) {
			$allergier_kategori->tags()->save(new Tag(array('name' => $allergi)));
		}


		$andre_kategori = new TagCategory(array('name' => 'Andre'));
		$andre_kategori->save();
		$andre = array(
			'Vegetar'
		);

		foreach ($andre as $ta) {
			$andre_kategori->tags()->save(new Tag(array('name' => $ta)));
		}


		return TagCategory::with('tags')->get();

	}

	public function getExportIngredients() {


		$domd = new DOMDocument();
		libxml_use_internal_errors(true);

		$domd->loadHTML(file_get_contents('file:///Users/patrickreck/Desktop/lande.html'));

		$links = $domd->getElementsByTagName('li');

		$names = array();
		foreach ($links as $index => $link) {

			array_push($names, ucfirst($link->nodeValue));

		}

		foreach ($names as $ingredient) {
			$origin = new Origin();
			$origin->name = $ingredient;
			$origin->save();
		}


		/*
		$links = $domd->getElementsByTagName('a');

		$names = array();
		foreach ($links as $index => $link) {
				array_push($names, ucfirst($link->nodeValue));
		}

		foreach ($names as $ingredient) {
			$ningredient = new Ingredient();
			$ningredient->name = $ingredient;
			$ningredient->save();
		}*/
	}

	public function getTestUppercase() {


	}

}

?>