<?php

class NotificationController extends Controller {

	private $limit = 4;

	public function getNotifications($user_id) {
		if (Sentry::check() && Sentry::getUser()->id == $user_id) {
			return $this->fetch(Sentry::getUser()->id);
		}

		return Response::make('You do not have permission to see this', 401);
	}

	public function getUnreadcount() {
		if (Sentry::check()) {
			$user = User::find(Sentry::getUser()->id);

			return $user->notifications()->unread()->count();
		}

		return Response::make('You are not logged in', 401);
	}

	private function fetch($id) {
		$notifications = Notification::where('user_id', '=', $id)
			->select('*', DB::raw('count(distinct(notifications.sender_id))-1 as additional_count'))
			->groupBy('notifiable_type', 'notifiable_id')
			->with('Sender')
			->with('Notifiable')
			->get();

		foreach ($notifications as $notification) {
			if ($notification->notifiable_type == 'RecipeRating' || $notification->notifiable_type == 'RecipeComment') {
				$notification->recipe = Recipe::where('id', $notification->notifiable->recipe_id)->with('images')->first();
			}
		}

		return $notifications;
	}
}

?>