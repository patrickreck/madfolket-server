<?php

class UserController extends Controller {

	public function getUser($user_id) {
		$validator = Validator::make(
			array(
				'user_id' => $user_id
			),
			array(
				'user_id' => 'required|exists:users,id'
			)
		);

		if ($validator->passes()) {
			return User::with('recipes', 'recipes.images')->find($user_id);
		} else {
			return Response::make('This user does not exist', 404);
		}
	}

	public function getBadges($user_id) {
		$validator = Validator::make(
			array(
				'user_id' => $user_id
			),
			array(
				'user_id' => 'required|exists:users,id'
			)
		);

		if ($validator->passes()) {
			return User::find($user_id)->badges;
		} else {
			return Response::make('This user does not exist', 404);
		}
	}

	public function getFeed($user_id) {
		$validator = Validator::make(
			array(
				'user_id' => $user_id
			),
			array(
				'user_id' => 'required|exists:users,id'
			)
		);

		if ($validator->passes()) {

			$user = User::find($user_id);
			$recipes = Recipe::with('images')->where('user_id', '=', $user->id)->get();
			$badges = $user->badges;

			return array('recipes' => $recipes, 'badges' => $badges);
		} else {
			return Response::make('This user does not exist', 404);
		}
	}

}

?>