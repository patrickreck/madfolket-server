<?php

class RecipeController extends Controller {

	public function getIndex() {

		$recipes = Recipe::with('images')->orderBy('id', 'DESC')->get();

		return $recipes;

	}


	public function getRecipe($recipe_id) {

		$recipe = Recipe::with('user', 'user.badges', 'category', 'origin', 'images', 'ingredients', 'steps', 'tags')
			->with(array('comments' => function ($query) {
				$query->with('user');
				$query->orderBy('created_at', 'desc');
				$query->take(3);
			}))
			->find($recipe_id);

		if ($recipe) {

			$extras = Recipe::with('images')->where('id', '!=', $recipe->id)->orderByRaw('RAND()')->take(3)->get();

			$recipe->addView();

			return array('recipe' => $recipe, 'extras' => $extras);
		} else {
			return Response::make('The recipe does not exist', 404);
		}
	}


	public function getMore($skip, $search = null) {
		$recipeQuery = Recipe::with('images')->orderBy('id', 'DESC')->skip($skip)->take(28);

		if ($search) {
			$request = new SearchRequest();
			$request->search = $search;
			if (Sentry::check()) {
				$request->user_id = Sentry::getUser()->id;
			}
			$request->save();

			$recipeQuery->where('name', 'LIKE', '%' . $search . '%');
		}

		return $recipeQuery->get();
	}

	public function getMoreComments($recipe_id, $skip) {
		$validator = Validator::make(
			array(
				'recipe_id' => $recipe_id
			),
			array(
				'recipe_id' => 'required|exists:recipes,id'
			)
		);

		if ($validator->passes()) {
			return RecipeComment::with('user')->where('recipe_id', '=', $recipe_id)->orderBy('created_at', 'desc')->skip($skip)->take(3)->get();
		}
	}

	public function postRate() {

		$validator = Validator::make(
			array(
				'recipe_id' => Input::get('recipe_id'),
				'rating'    => Input::get('rating')
			),
			array(
				'recipe_id' => 'required|exists:recipes,id',
				'rating'    => 'between:1,5|integer'
			)
		);

		if ($validator->passes()) {

			if (!Sentry::check())
				return Response::make('You are not logged in', 401);

			// Get input
			$user = User::find(Sentry::getUser()->id);
			$recipe = Recipe::find(Input::get('recipe_id'));
			$new_rating = Input::get('rating');

			// Check if user already rated
			if ($current_rating = RecipeRating::where('recipe_id', '=', $recipe->id)->where('user_id', '=', $user->id)->first()) {

				// If the new rating isn't null (user deletion), update. If it is, delete the existing one
				if (Input::get('rating') !== null) {
					$current_rating->rating = $new_rating;
					$current_rating->save();
				} else {
					$current_rating->delete();

					return $recipe->rating;
				}

			} // Otherwise create a new rating object
			else {
				$current_rating = RecipeRating::create(array(
					'rating' => Input::get('rating')
				));
				$current_rating->user()->associate($user);
				$current_rating->recipe()->associate($recipe);
				$current_rating->save();
			}

			//Send notification
			$notification = new Notification();
			$notification
				->to($recipe->user)
				->from($user)
				->regarding($current_rating);
			$notification->save();

		} else {
			return Response::make('Validation failed', 404);
		}
	}

	public function postSaveComment() {
		$validator = Validator::make(
			array(
				'recipe_id' => Input::get('recipe_id'),
				'comment'   => Input::get('comment')
			),
			array(
				'recipe_id' => 'required|exists:recipes,id',
				'comment'   => 'required|between:1,500'
			)
		);

		if ($validator->passes()) {

			if (!Sentry::check())
				return Response::make('You are not logged in', 401);

			$recipe = Recipe::find(Input::get('recipe_id'));
			$user = User::find(Sentry::getUser()->id);
			$comment = new RecipeComment(array('comment' => Input::get('comment')));
			$comment->recipe()->associate($recipe);
			$comment->user()->associate($user);
			$comment->save();

			//Send notification
			$notification = new Notification();
			$notification
				->to($recipe->user)
				->from($user)
				->regarding($comment);
			$notification->save();

			return $comment;
		} else {
			return Response::make('Validation failed', 404);
		}
	}

	public function postSave() {

		if (!Sentry::check())
			return Response::make('You are not logged in', 401);

		// Set validation rules
		$validator = Validator::make(
			array(
				'name'         => Input::get('name'),
				'introduction' => Input::get('description'),
				'freezable'    => Input::get('freezable'),
				'origin'       => Input::get('origin'),
				'category'     => Input::get('category'),
				'tags'         => Input::get('tags'),
				'ingredients'  => Input::get('ingredients'),
				'steps'        => Input::get('steps'),
				'images'       => Input::get('images'),
				'persons'      => Input::get('persons'),
				'hours'        => Input::get('hours'),
				'minutes'      => Input::get('minutes')
			),
			array(
				'name'         => 'required|between:2,90',
				'introduction' => 'max:1000',
				'freezable'    => 'required|boolean',
				'origin'       => 'required|exists:origins,id',
				'category'     => 'required|exists:categories,id',
				'tags'         => 'is_tags',
				'ingredients'  => 'is_ingredients',
				'steps'        => 'required|is_steps',
				'images'       => 'recipe_images_exists',
				'persons'      => 'required|integer|between:1,20',
				'hours'        => 'required|integer|between:0,99',
				'minutes'      => 'required|integer|between:0,59'
			)
		);

		if ($validator->passes()) {

			$recipe = new Recipe();

			$recipe->name = Input::get('name');
			$recipe->introduction = nl2br(Input::get('introduction'));
			$recipe->origin_id = Input::get('origin');
			$recipe->category_id = Input::get('category');
			$recipe->persons = Input::get('persons');
			$recipe->hours = Input::get('hours');
			$recipe->minutes = Input::get('minutes');

			// Set freezable
			if (Input::get('freezable'))
				$recipe->freezable = 1;
			else
				$recipe->freezable = 0;

			// Save for attachments
			$recipe->save();

			// Attach tags
			foreach (Input::get('tags') as $input_tag) {
				$recipe->tags()->attach(Tag::find($input_tag));
			}

			// Attach ingredients
			foreach (Input::get('ingredients') as $input_ingredient) {

				// If the ingredients already exists, assign it to $ingredient
				if ($ingredient = Ingredient::where('name', 'LIKE', $input_ingredient['name'])->first()) {
				} // If not create a new one and assign it to $ingredient
				else {
					$ingredient = new Ingredient();
					$ingredient->name = ucfirst($input_ingredient['name']);
					$ingredient->save();
				}
				$recipe->ingredients()->attach($ingredient, array('amount' => $input_ingredient['amount'], 'unit_id' => $input_ingredient['unit']));
			}

			// Attach steps
			foreach (Input::get('steps') as $input_step) {
				$step = new RecipeStep();
				$step->description = $input_step['description'];

				// If timer is set on step, set it
				if (isset($input_step['timer'])) {
					$step->timer = $input_step['timer'];
				}

				$recipe->steps()->save($step);
			}

			// Attach images
			mkdir('images/recipe_images/' . $recipe->id);
			foreach (Input::get('images') as $input_image) {

				// Create new RecipeImage object 
				$image = new RecipeImage();
				$image->path = $input_image['path'];

				// Move the image to the appropriate path
				$temporaryPath = 'images/recipe_images/temporary/' . $input_image['path'];
				$folder = 'images/recipe_images/' . $recipe->id . '/';
				$newPath = $folder . $input_image['path'];
				rename($temporaryPath,
					$newPath);

				// Set the main image of the recipe
				if ($input_image['main'])
					$image->main = 1;

				$recipe->images()->save($image);
			}

			// Get the User object from Sentry
			$user = User::find(Sentry::getUser()->id);
			$user->recipes()->save($recipe);

			// Check if any badges should be granted
			Event::fire('badges.recipe', array($user));

			return $recipe;

		}

		return $validator->messages();

		return Response::make('An error occured', 400);
	}

	public function postSaveImage() {

		$validator = Validator::make(
			array(
				'image' => Input::file('file')
			),
			array(
				'image' => 'required|image|mimes:jpeg,bmp,png|max:2500'
			)
		);

		if ($validator->passes()) {

			$file = Input::file('file');

			$newFileName = getGuid() . '.' . $file->getClientOriginalExtension();

			$file->move('images/recipe_images/temporary/', $newFileName);

			return Response::make(array('path' => $newFileName), 200);
		} else {
			return Response::make(array("Not an image"), 400);
		}
	}

	public function postSearchRecipes() {

		$recipes = Recipe::with('images')->orderBy('id', 'DESC');

		if (Input::get('categories')) {
			$recipes->whereIn('category_id', Input::get('categories'));
		}

		if (Input::get('origins')) {
			$recipes->whereIn('origin_id', Input::get('origins'));
		}

		return $recipes->get();
	}

}

?>