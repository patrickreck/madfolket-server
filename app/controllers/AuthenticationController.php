<?php


use \Facebook\FacebookSession;
use \Facebook\FacebookRedirectLoginHelper;
use \Facebook\FacebookJavaScriptLoginHelper;
use \Facebook\FacebookRequest;

class AuthenticationController extends Controller {

	public function getCheckLogin() {
		if (Sentry::check())
			return User::find(Sentry::getUser()->id);

		return Response::make('You are not logged in', 401);
	}

	public function postLogin() {

		FacebookSession::setDefaultApplication(Config::get('facebook.app_id'), Config::get('facebook.app_secret'));

		$session = new FacebookSession(Input::get('token'));

		// Get profile picture
		$request = new FacebookRequest($session, 'GET', '/me/picture', array(
			'redirect' => false,
			'height'   => '400',
			'type'     => 'normal',
			'width'    => '400',
		));
		$profile_picture_url = $request->execute()->getGraphObject()->getProperty('url');


		// Get the basic user data
		$request = new FacebookRequest($session, 'GET', '/me');
		$graphObject = $request->execute()->getGraphObject();

		$facebook_id = $graphObject->getProperty('id');

		$user = null;
		if (count($users = User::where('facebook_id', '=', $facebook_id)->get()) == 1) {
			$user = Sentry::findUserById($users->toArray()[0]['id']);
		} else {
			$user = $this->registerUser($graphObject, $profile_picture_url);
		}


		Sentry::login($user);

		return $user;
	}

	private function registerUser($graphObject, $profile_picture_url) {

		$facebook_id = $graphObject->getProperty('id');
		$first_name = $graphObject->getProperty('first_name');
		$last_name = $graphObject->getProperty('last_name');
		$email = $graphObject->getProperty('email');


		$user = Sentry::createUser(array(
			'facebook_id' => $facebook_id,
			'first_name'  => $first_name,
			'last_name'   => $last_name,
			'email'       => $email,
			'password'    => str_random(40),
			'activated'   => 1,
		));

		mkdir('images/profile_pictures/' . $user->id);
		$img = 'images/profile_pictures/' . $user->id . '/1.jpg';
		file_put_contents($img, file_get_contents($profile_picture_url));

		$user->profile_picture = '1.jpg';
		$user->save();

		return $user;

	}

	private function countFilesInDirectory($directory) {

		$i = 0;

		if ($handle = opendir($directory)) {
			while (($file = readdir($handle)) !== false) {
				if (!in_array($file, array('.', '..')) && !is_dir($directory . $file))
					$i++;
			}
		}

		return $i;

	}

	public function postLogout() {
		Sentry::logout();

		return Response::make('Logged out', 200);
	}


}

?>