<?php

Validator::extend('is_steps', function($attribute, $value, $parameters) {

	foreach ($value as $step) {

		if (!isset($step['timer']))
			$step['timer'] = null;

		$validator = Validator::make(
			array(
				'description' => $step['description'],
				'timer'		  => $step['timer']
			),
			array(
				'description' => 'required|between:2,1000',
				'timer'		  => 'numeric|integer'
			)
		);

		if ($validator->fails()) {
			return false;
		}

	}

	return true;
});

Validator::extend('is_tags', function($attribute, $value, $parameters) {

	foreach ($value as $tag) {

		$validator = Validator::make(
			array(
				'id' => $tag
			),
			array(
				'id' => 'required|exists:tags,id'
			)
		);

		if ($validator->fails()) {
			return false;
		}

	}

	return true;
});

Validator::extend('is_ingredients', function($attribute, $value, $parameters) {

	foreach ($value as $ingredient) {

		$validator = Validator::make(
			array(
				'name'   => $ingredient['name'],
				'amount' => $ingredient['amount'],
				'unit'	 => $ingredient['unit']
			),
			array(
				'name'	 => 'required|between:2,30',
				'amount' => 'required|numeric|between:0.1,9999',
				'unit'	 => 'required|exists:units,id'
			)
		);

		if ($validator->fails()) {
			return false;
		}

	}

	return true;
});

Validator::extend('recipe_images_exists', function($attribute, $value, $parameters) {

	foreach ($value as $image) {

		if (!file_exists('images/recipe_images/temporary/' . $image['path']))
			return false;

	}

	return true;
});

?>